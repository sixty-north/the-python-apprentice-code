# text_analyzer.py

class TextAnalysisTests(unittest.TestCase):
    . . .
    def test_no_such_file(self):
        "Check the proper exception is thrown for a missing file."
        with self.assertRaises(IOError):
            analyze_text('foobar')
