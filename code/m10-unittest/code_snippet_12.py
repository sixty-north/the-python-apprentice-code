# text_analyzer.py

def analyze_text(filename):
    """Calculate the number of lines and characters in a file.

    Args:
      filename: The name of the file to analyze.

    Raises:
      IOError: If ``filename`` does not exist or can't be read.

    Returns: The number of lines in the file.
    """
    with open(filename, 'r') as f:
        return sum(1 for _ in f)
