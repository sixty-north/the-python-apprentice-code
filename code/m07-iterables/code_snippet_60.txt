>>> tuesday = [2, 2, 3, 7, 9, 10, 11, 12, 10, 9, 8, 8]
>>> for temps in zip(sunday, monday, tuesday):
...     print("min={:4.1f}, max={:4.1f}, average={:4.1f}".format(
...            min(temps), max(temps), sum(temps) / len(temps)))
...
min= 2.0, max=13.0, average= 9.0
min= 2.0, max=14.0, average=10.0
min= 3.0, max=15.0, average=10.7
min= 7.0, max=15.0, average=12.0
min= 9.0, max=17.0, average=14.0
min=10.0, max=21.0, average=17.0
min=11.0, max=22.0, average=18.0
min=12.0, max=22.0, average=18.7
min=10.0, max=23.0, average=18.3
min= 9.0, max=22.0, average=17.3
min= 8.0, max=20.0, average=15.7
min= 8.0, max=18.0, average=14.3
