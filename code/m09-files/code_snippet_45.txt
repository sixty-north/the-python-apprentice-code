>>> def words_per_line(flo):
...    return [len(line.split()) for line in flo.readlines()]
