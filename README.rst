===========================
The Python Apprentice: Code
===========================

Code examples and other snippets from Sixty North's `The Python Apprentice <https://leanpub.com/python-apprentice/>`_.

These snippets are automatically extracted from the markdown source for the book. As such, they're not particularly
organized or curated; they're simply a linear dump of code examples. Hopefully, though, they'll help readers who are
looking for the code examples from the book in a more readily usable form.

Using the snippets
==================

The ``code`` directory contains all of the code examples from the book. It's organized into one directory per chapter,
and inside each of those directories the extracted snippets are listed in the order they're found in the book. We've
attempted to give sensible extensions to the snippets, but they may not all be correct.
